// @file gulpfile.js
var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
//var plumber = require('gulp-plumber');

// sass task
gulp.task('sass', function(){
  return sass('scss/',{
    style: 'expanded',
    compass: true
  })
  .on('error', sass.logError)
  .pipe(gulp.dest('css'));
});

// watch task
gulp.task('watch', function () {
  gulp.watch('scss/**/*.scss', ['sass']);
});
